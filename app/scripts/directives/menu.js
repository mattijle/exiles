'use strict';
function msToTime(duration) {
    var seconds = parseInt((duration/1000)%60,10), 
        minutes = parseInt((duration/(1000*60))%60,10), 
        hours = parseInt((duration/(1000*60*60))%24,10);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + " hrs " + minutes + " mins " + seconds + " secs";
}


angular.module('exilesApp')
  .directive('myMenu', function () {
    return {
        templateUrl: 'partials/menu.html',
        restrict: 'A',
        scope: {},
        controller: function($scope) {
            $scope.upcoming = [];
            $scope.live = [];
            $.get('/api/events/list',function(data) {
                var events = data;
                var now = +new Date();
                angular.forEach(events, function(event){
                    event.startAt = Date.parse(event.startAt);
                    event.endAt = Date.parse(event.endAt);
                    event.startsIn = msToTime(event.startAt - now);
                    
                    if(event.startAt  < (now + 16*(1000*60*60))) {
                        if(event.startAt > now) {
                                event.id = event.id.replace(/ *\([^)]*\) */g, "");
                                $scope.upcoming.push(event);
                        } else if(event.startAt < now && event.endAt > now) {
                                event.id = event.id.replace(/ *\([^)]*\) */g, "");
                                $scope.live.push(event);
                        }else {

                        }
                    } 
                });
                $scope.$apply(function(){
                    $scope.upcoming.sort(function(a,b){
                        if(a.startAt < b.startAt) return -1;
                        if(a.startAt > b.startAt) return 1;
                        return 0;
                    });
                });
            });
        }
      };
  });
