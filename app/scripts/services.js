'use strict';

angular.module('exilesApp')
    .factory('eventFactory', function($http, $q){
        return {
            eventData : {},
            error: '',
            apiPath : '/api/events/',
            asyncFunc : function(path) {
                var deferred = $q.defer();

                $http.get(this.apiPath + path).success(function(data){
                    deferred.resolve(data);
                }).error(function(){
                    deferred.reject('An error occured while fetching event.');
                });
                return deferred.promise;
            },
            getLadder: function(ep) {
                return this.asyncFunc(ep);
            }
        };
    });
