'use strict';

angular.module('exilesApp')
  .controller('EventCtrl', function ($scope, $filter,$routeParams, eventFactory) {

    $scope.episode = $routeParams.eventId;
    var eventData = {};
    $scope.event = {};
    
    $scope.currentPage = 0;
    $scope.pagedEntries = [];
    $scope.filteredItems = [];
    $scope.entriesPerPage = 50;
    $scope.highlight = "";
    $scope.classes =[
              {'value': '','text':'All'},
              {'value': 'Ranger','text':'Ranger'},
              {'value': 'Shadow','text':'Shadow'},
              {'value': 'Marauder','text':'Marauder'},
              {'value': 'Templar','text':'Templar'},
              {'value': 'Witch','text':'Witch'},
              {'value': 'Duelist','text':'Duelist'},
              {'value': 'Scion', 'text':'Scion'}
      ];
    $scope.charClass = $scope.classes[0].value;
    $scope.isHighlighted = function(accName,charName){
        if($scope.highlight !== ""){
            if(searchMatch(accName,$scope.highlight)|| 
            searchMatch(charName,$scope.highlight)) {
                return "highlight-row";
            }
        }
    };    
    var searchMatch = function(haystack,needle) {
        if(!needle)
            return true;
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };
    $scope.search = function() {
        $scope.filteredItems = $filter('filter')(eventData.ladder, function(entry) {
            if(searchMatch(entry.character.class, $scope.charClass)){
                return true;
            }
            return false;
        });
        $scope.groupToPages();
    };
    $scope.groupToPages = function() {
        $scope.pagedEntries = [];
        for(var i =0;i<$scope.filteredItems.length;i++){
            if(i % $scope.entriesPerPage === 0) {
                $scope.pagedEntries[Math.floor(i/ $scope.entriesPerPage)] = [$scope.filteredItems[i]];
            } else {
                $scope.pagedEntries[Math.floor(i/$scope.entriesPerPage)].push($scope.filteredItems[i]);
            }
        }
    };
    $scope.range = function(start,end) {
        var ret = [];
        if(!end) {
            end = start;
            start = 0;
        }
        for(var i = start;i<end;i++) {
            ret.push(i);
        }
        return ret;
    };
    $scope.prevPage = function() {
        if($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };
    $scope.nextPage = function() {
        if($scope.currentPage < $scope.pagedEntries.length - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = function() {
        $scope.currentPage = this.n;
    }
    var getLadder = function() {
        eventFactory.getLadder($scope.episode).then(function(data){
            eventData = data;
            $scope.event.id = data.id;
            $scope.event.url = data.url;
            $scope.event.rules = data.rules;
            $scope.search();
        },function(err){ 
            $scope.error = err;
        });
    };
    getLadder();
    
    setInterval(function(){
        getLadder();
        },60000);
  });
