/*jslint node:true */
"use strict";

var mongoose = require('mongoose'),
    poe = require('poejs'),
    Thing = mongoose.model('Thing'),
    League = mongoose.model('League'),
    async = require('async'),
    redis = require('redis'),
    client = redis.createClient();

exports.listEvents = function(req,res) {
  return League.find({event: true},'id episode startAt endAt',function(err,leagues){
    if(!err) {
      return res.json(leagues);
    } else {
      return res.send(err);
    }
  });
};
exports.getEvent = function(req, res) {
  var league = req.params.id;
  League.findOne({episode: league },function(err,leagues){
    return res.json(leagues);
  });
};
exports.getLeague = function(req, res) {
    var league = req.params.id;
//    client.hgetall("live",league, function(err, data){
//        if(data !== null){
//            console.log('Found data from Redis, using it.');
//            return res.json(data);
//        }else {
            League.findOne({id: league }, function(err, leagues){
                return res.json(leagues);
            });
//        }
//    });
};
