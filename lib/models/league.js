'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LeagueSchema = new Schema({
    id: String,
    episode: String,
    description: String,
    url: String,
    registerAt: String ,
    startAt: String,
    endAt: String,
    rules: [],
    event: Boolean,
    ladder: []

  });

mongoose.model('League', LeagueSchema);
