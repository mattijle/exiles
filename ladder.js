/* jslint node: true */
'use strict';

var poe = require('poejs'),
    path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    redis = require('redis'),
    client = redis.createClient(),
    async = require('async');

// Connect to database
var db = require('./lib/db/mongo');
var events = [];
var watch_list = {};
// Bootstrap models
var modelsPath = path.join(__dirname, 'lib/models');
fs.readdirSync(modelsPath).forEach(function (file) {
  require(modelsPath + '/' + file);
});

var League = mongoose.model('League');

var listEvents = function(call) {
    League.find({event: true},'id episode startAt endAt',function(err,leagues){
    if(!err) {
        call(null,leagues);
    } else {
        call(err,null);
    }
  });
};

var fetchEvents = function(call) {
    poe.getEvents().then(function(data){
        if(!data.error) {
            call(null,data);
        } else {
            call(data.error,null);
        }
    });
};

var updateEvent = function(e) {
    var start = 0;
    var total = 15000;
    var ladder = [];
    League.findOne({id: e.id},function(err,league){
        recur(league,start,total);
    });
    function recur(ev,s,t) {
        poe.getLadder(ev.id,s).then(function(d){
            if(!d.error){
                console.log(ev.id);
                total = d.total;
                console.log(s,total);
                s += 200;
                ladder = ladder.concat(d.entries);
                if(s < total){ 
                    setTimeout(function(){
                        recur(ev,s,t);
                    },1000);
                } else {
                    console.log('Time to update database.');
                    ev.ladder = ladder;
                    client.hset('live',ev.id,ev,function(err,d){
                        if(err) { 
                            console.log(err);
                        }
                    });
                    League.update({_id: ev._id},{ladder: ev.ladder},{upsert:true}, updateCallBack);
                }
            }else {
                console.log(d.error.code,d.error.message);
            }
        });
    }
};

var updateCallBack = function(err,num,n) {
    if(!err) {
        console.log(num,n);
    } else {
        throw err;
    }
};

var addToWatchList = function(ev) {
    console.log('Adding event to watch list', ev.id);
    watch_list[ev.id] = setInterval(function(){
        console.log('Updating ladder for ep ' + ev.id);
        updateEvent(ev);
        var epend = Date.parse(ev.endAt);
        var now = +new Date();
        if((epend-now)<0){
            removeFromWatchList(ev);
        }
    },120000);
};
var removeFromWatchList = function(ev) {
    console.log('Removing event '+ev.id+' from watchlist.');
    clearInterval(watch_list[ev.id]);
    delete watch_list[ev.id];
};
var watchQ = {};
var watchQueue = function(ev) {
    var to = Date.parse(ev.startAt) - new Date();
    if(to < 0 ) {
        to = 0;
    }
    if(watchQ[ev.id] !== true){
        watchQ[ev.id] = true;
        console.log('Adding event ' + ev.id + ' to watchQ');
        setTimeout(function(){
            addToWatchList(ev);    
        },to);
    }
};


var main = function() {
    fetchEvents(function(e,d){
        var regExp = /\(([^)]+)\)/;                     
        if(!e) {
            for(var i in d) {
                var ep = regExp.exec(d[i].id);
                if(ep !== null) { 
                    d[i].episode = ep[1];
                }else {
                    d[i].episode = d[i].id;
                }
                League.update({id: d[i].id},d[i],{upsert:true}, updateCallBack);        
            }
            listEvents(function(ee,dd) {
                if(!e) {
                    for(var i in dd) {
                        var now = +new Date();
                        var epstart = Date.parse(dd[i].startAt);
                        var epend = Date.parse(dd[i].endAt);
                        if((epend - now) > 0) {
                            watchQueue(dd[i]);
                        }
                    }
                }
                
            });
        }
    });
};

main();
setInterval(main,1000*60*30);
